import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateComponent } from './private.component';
import { CoursesModule } from './courses/courses.module';
import { UserModule } from '../user/user.module';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared.module';
import { GraphicModule } from './graphic/graphic.module';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { RouterOutlet } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

xdescribe('PrivateComponent', () => {
  let component: PrivateComponent;
  let fixture: ComponentFixture<PrivateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PrivateComponent,
        DashboardComponent
      ],
      imports: [
        RouterTestingModule,
        GraphicModule,
        SharedModule,
        FormsModule,
        UserModule,
        CoursesModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Se crea el Private Component', () => {
    expect(component).toBeTruthy();
  });

  it('Se crea un router outlet en el Private Component', () =>  {
    const debugElement = fixture.debugElement.query( By.directive(RouterOutlet) );
    console.log(debugElement);
    expect( debugElement ).not.toBeNull();
  });
});
