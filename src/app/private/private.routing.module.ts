import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GraphicComponent } from './graphic/graphic.component';
import { PrivateComponent } from './private.component';
import { AuthGuard } from '../user/auth/guards/auth.guard';
import { CourseResolver } from './courses/services/course.resolver';
import { CourseComponent } from './courses/components/course/course.component';

export const routes: Routes = [
  {
    path: '',
    component: PrivateComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'dashboard' },
      { path: 'dashboard', component: DashboardComponent, },
      {
        path: 'course/:id',
        component: CourseComponent,
        resolve: {
          course: CourseResolver
        }
       },
      { path: 'graphics', component: GraphicComponent }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ], exports: [
    RouterModule
  ]
})
export class PrivateRoutingModule { }
