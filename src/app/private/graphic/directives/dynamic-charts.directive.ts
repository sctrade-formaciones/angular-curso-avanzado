import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appDynamicCharts]'
})
export class DynamicChartsDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
