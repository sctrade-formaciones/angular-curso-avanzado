import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doughnut',
  templateUrl: './doughnut.component.html',
  styleUrls: ['./doughnut.component.css']
})
export class DoughnutComponent implements OnInit {
  public data: any[] = [
    {
      name: 'Dato 1',
      value: 400
    },
    {
      name: 'Dato 2',
      value: 600
    }
  ];
  public doughnutChartLabels: string[] = [this.data[0].name, this.data[1].name];
  public doughnutChartData: number[] = [this.data[0].value, this.data[1].value];
  public doughnutChartType = 'doughnut';

  constructor() { }

  ngOnInit() {
  }

}
