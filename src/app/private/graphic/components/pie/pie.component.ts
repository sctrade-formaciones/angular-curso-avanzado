import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.css']
})
export class PieComponent implements OnInit {
  public data: any[] = [
    {
      name: 'Dato 1',
      value: 100
    },
    {
      name: 'Dato 2',
      value: 900
    }
  ];
  public pieChartLabels: string[] = [this.data[0].name, this.data[1].name];
  public pieChartData: number[] = [this.data[0].value, this.data[1].value];
  public pieChartType = 'pie';

  constructor() { }

  ngOnInit() {
  }

}
