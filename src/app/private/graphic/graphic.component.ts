import { Component, OnInit, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { DynamicChartsDirective } from './directives/dynamic-charts.directive';
import { DynamicItem } from './model/dynamic-item.model';
import { PieComponent } from './components/pie/pie.component';
import { DoughnutComponent } from './components/doughnut/doughnut.component';

@Component({
  selector: 'app-graphic',
  templateUrl: './graphic.component.html',
  styleUrls: ['./graphic.component.css']
})
export class GraphicComponent implements OnInit {
  @ViewChild(DynamicChartsDirective) dCharts: DynamicChartsDirective;
  charts: DynamicItem[] = [];
  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.charts.push({
       component: PieComponent,
       data: null
    }, {
      component: DoughnutComponent,
       data: null
    });
    this.loadComponent(this.charts[0]);
  }

  loadComponent(item: DynamicItem) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(item.component);
    const viewContainerRef = this.dCharts.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent(componentFactory);
  }

  changeChart(chartPosition: number) {
    this.loadComponent(this.charts[chartPosition]);
  }

}
