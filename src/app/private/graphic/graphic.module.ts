import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BarComponent } from './components/bar/bar.component';
import { DoughnutComponent } from './components/doughnut/doughnut.component';
import { PieComponent } from './components/pie/pie.component';
import { GraphicComponent } from './graphic.component';
import { ChartsModule } from 'ng2-charts';
import { DynamicChartsDirective } from './directives/dynamic-charts.directive';
@NgModule({
  declarations: [
    BarComponent,
    DoughnutComponent,
    PieComponent,
    GraphicComponent,
    DynamicChartsDirective
  ],
  imports: [
    CommonModule,
    ChartsModule
  ], entryComponents: [
    BarComponent,
    DoughnutComponent,
    PieComponent,
  ]
})
export class GraphicModule { }
