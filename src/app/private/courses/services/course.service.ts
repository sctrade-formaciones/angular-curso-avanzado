import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Course } from 'src/app/model/Course.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<Course[]> {
    return this.http.get('http://localhost:3000/api/courses').pipe( map((courses: Course[]) => {
      return courses;
    }));
  }

  findCourseById(id: number): Observable<Course> {
    const params: HttpParams = new HttpParams().set('id', id.toString());
    return this.http.get('http://localhost:3000/api/course',  { params }).pipe( map((course: Course) => {
      return course;
    }));
  }
}
