import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Course } from './../../../model/Course.model';
import { CourseService } from './course.service';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/reducers';
import { selectCourseById } from '../course.selector';
import { tap, filter, first } from 'rxjs/operators';
import { CourseRequested } from '../course.actions';



@Injectable()
export class CourseResolver implements Resolve<Course> {

    constructor(
      private store: Store<AppState>
      ) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Course> {
        const courseId: number = route.params['id'];
        return this.store.pipe(
          select(selectCourseById(courseId)),
          tap( course => {
            if (!course) {
              this.store.dispatch(new CourseRequested({courseId}));
            }
          }),
          filter(course =>  !!course),
          first() // Para completar el observable y la transición del router. Sólo terminará el router cuando termine el observable.
        );
    }

}
