import { Component, OnInit } from '@angular/core';
import { Course } from '../../../../model/Course.model';
import { CourseService } from '../../services/course.service';
import { AppState } from 'src/app/reducers';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectAllCourses } from '../../course.selector';
import { AllCoursesRequested } from '../../course.actions';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {
  courses$: Observable<Course[]>;
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.dispatch(new AllCoursesRequested());
    this.courses$ = this.store.pipe(select(selectAllCourses));
  }

}
