import { Action } from '@ngrx/store';
import { Course } from '../../model/Course.model';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { CourseActionTypes, CourseActions, AllCoursesLoaded } from './course.actions';
import { CoursesState } from './course.reducer';

export interface CoursesState extends EntityState<Course> {
  allCoursesLoaded: boolean;
}

export const adapter: EntityAdapter<Course> = createEntityAdapter<Course>();

export const initialCoursesState: CoursesState = adapter.getInitialState({
  allCoursesLoaded: false
});

export function reducer(state: CoursesState = initialCoursesState, action: CourseActions): CoursesState {
  switch (action.type) {
    case CourseActionTypes.CourseLoaded:
      // Se utiliza el adapter para modificar el estado
      return adapter.addOne(action.payload.course, state);
    case CourseActionTypes.AllCoursesLoaded:
      return adapter.addAll(action.payload.courses, {...state, allCoursesLoaded: true});
    default:
      return state;
  }
}


export const {
  selectAll,
  selectEntities,
  selectIds
} = adapter.getSelectors();
