import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromCourse from './course.reducer';
import { CourseCardComponent } from './components/course-card/course-card.component';
import { RouterModule } from '@angular/router';
import { CourseResolver } from './services/course.resolver';
import { EffectsModule } from '@ngrx/effects';
import { CourseEffects } from './course.effects';
import { CourseListComponent } from './components/course-list/course-list.component';
import { CourseComponent } from './components/course/course.component';
@NgModule({
  declarations: [
    CourseCardComponent,
    CourseListComponent,
    CourseComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    StoreModule.forFeature('course', fromCourse.reducer),
    EffectsModule.forFeature([CourseEffects])
  ], providers: [
    CourseResolver
  ], exports: [
    CourseCardComponent,
    CourseListComponent
  ]
})
export class CoursesModule { }
