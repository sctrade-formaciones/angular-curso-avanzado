import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Action, Store, select } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { CourseActionTypes, CourseLoaded, AllCoursesRequested, AllCoursesLoaded } from './course.actions';
import { mergeMap, map, filter, withLatestFrom, catchError } from 'rxjs/operators';
import { CourseService } from './services/course.service';
import { Course } from './../../model/Course.model';
import { AppState } from './../../reducers/index';
import { allCoursesLoaded } from './course.selector';

@Injectable()
export class CourseEffects {

  @Effect()
  loadCourse$: Observable<Action> = this.actions$
    .pipe(
      ofType(CourseActionTypes.CourseRequested),
      mergeMap((action: any) => this.courseService.findCourseById(action.payload.courseId)),
      map((course: Course) => new CourseLoaded({ course }))
    );

  @Effect()
  loadAllCourses$ = this.actions$
    .pipe(
      ofType<AllCoursesRequested>(CourseActionTypes.AllCoursesRequested),
      withLatestFrom(this.store.pipe(select(allCoursesLoaded))),
      filter(([action, coursesLoaded]) => !coursesLoaded),
      mergeMap(() => this.courseService.findAll()),
      map(courses => new AllCoursesLoaded({ courses })),
      catchError(err => {
        console.log('error loading all courses ', err);
        return throwError(err);
      })
    );

  constructor(
    private actions$: Actions,
    private courseService: CourseService,
    private store: Store<AppState>
  ) { }
}
