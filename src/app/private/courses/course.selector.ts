import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CoursesState } from './course.reducer';
import * as fromCourse from './course.reducer';
import { filter } from 'rxjs/operators';
export const selectCoursesState = createFeatureSelector<CoursesState>('course');

export const selectCourseById = (courseId: number) => createSelector(
  selectCoursesState,
  coursesState => coursesState.entities[courseId] // Devuelve undefined si no lo encuentra
);

export const selectAllCourses = createSelector(
  selectCoursesState,
  fromCourse.selectAll
);

export const allCoursesLoaded = createSelector(
  selectCoursesState,
  coursesState => coursesState.allCoursesLoaded
);
