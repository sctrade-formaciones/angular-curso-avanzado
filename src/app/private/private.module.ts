import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PrivateRoutingModule } from './private.routing.module';
import { GraphicModule } from './graphic/graphic.module';
import { PrivateComponent } from './private.component';
import { SharedModule } from './shared/shared.module';

import { FormsModule } from '@angular/forms';
import { UserModule } from '../user/user.module';
import { CoursesModule } from './courses/courses.module';

@NgModule({
  declarations: [
    DashboardComponent,
    PrivateComponent
  ],
  imports: [
    CommonModule,
    PrivateRoutingModule,
    GraphicModule,
    SharedModule,
    FormsModule,
    UserModule,
    CoursesModule
  ]
})
export class PrivateModule { }
