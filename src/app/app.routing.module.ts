import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './public/public.module#PublicModule'},
  { path: 'user', loadChildren: './user/user.module#UserModule'},
  { path: 'app', loadChildren: './private/private.module#PrivateModule'},
  { path: '**', pathMatch: 'full', redirectTo: '404'}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
