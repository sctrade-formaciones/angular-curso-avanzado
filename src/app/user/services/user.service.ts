import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from 'src/app/model/user.model';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {
  }

  login(username: string, password: string): Observable<User> {
    return this.http.post('http://localhost:3000/api/login', {username, password}).pipe(map((response: any) => {
      return response.user as User;
    }));
  }

  getToken(): string {
    return localStorage.getItem('user');
  }
}
