import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Login, AuthActionTypes, Logout } from './auth.actions';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { defer, of } from 'rxjs';

@Injectable()
export class AuthEffects {

  @Effect({dispatch: false})
  login$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.LoginAction),
    tap((action: any) => {
      localStorage.setItem('user', JSON.stringify(action.payload.user));
      this.router.navigateByUrl('/app');
    })
  );

  @Effect({dispatch: false})
  logout$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.LogoutAction),
    tap((action: any) => {
      localStorage.removeItem('user');
      this.router.navigateByUrl('/');
    })
  );

  @Effect()
  init$ = defer(() => {
    const userData = localStorage.getItem('user');
    if (userData) {
      return of(new Login({'user': JSON.parse(userData)}));
    } else {
      return <any>of(new Logout());
    }
  });
  constructor(private actions$: Actions, private router: Router) {}

}
