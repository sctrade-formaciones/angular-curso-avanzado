import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import * as fromAuth from '../auth/auth.reducer';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { By } from '@angular/platform-browser';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        ReactiveFormsModule,
        HttpClientModule,
        StoreModule.forRoot({'auth': fromAuth.authReducer}),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges(); // Dispara la detección de cambios en los elementos del componente
  });

  it('Test de creación del componente de Login', () => {
    expect(component).toBeTruthy();
  });

  it('Test básico de comprobación del título de la card del componente', () => {
    const elem: HTMLElement = fixture.debugElement.query( By.css('.panel-title') ).nativeElement;
    expect( elem.innerHTML ).toContain('Inicio de sesión');
  });

  it('Test sobre los reactiveForms del Login', () => {
    const usernameControl = component.formGroup.get('username');
     usernameControl.setValue('test');
    const elem = fixture.debugElement.query( By.css('.btn-success') );
    // elem.triggerEventHandler('click', null);
    expect( component.formGroup.value.username ).not.toBe('');

  });

});
