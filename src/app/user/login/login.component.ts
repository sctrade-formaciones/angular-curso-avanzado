import { Component, OnInit } from '@angular/core';
import { UserService } from './../services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from './../../reducers/index';
import { Login } from '../auth/auth.actions';
import { tap } from 'rxjs/operators';
import { noop } from 'rxjs';
import { User } from 'src/app/model/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;
  constructor(private _userService: UserService, private store: Store<AppState>) { }

  ngOnInit() {
    this.formGroup = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  login() {
    if (this.formGroup.valid) {
      this._userService.login(this.formGroup.value.username, this.formGroup.value.password)
        .pipe(
          tap((user: User) => {
            this.store.dispatch(new Login({ user }));
          })
        )
        .subscribe(
          noop,
          () => alert('Login failed!'));
    }
  }
}
