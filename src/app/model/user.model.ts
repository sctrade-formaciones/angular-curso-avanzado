export interface User {
  id: number;
  name: string;
  sname: string;
  email: string;
  password: string;
  token: string;
}
