export interface Course {
  id: number;
  name: string;
  level: string;
  teacher: string;
}
