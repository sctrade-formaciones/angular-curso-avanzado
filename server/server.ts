const express = require('express');
const bodyParser = require('body-parser');
const encrypt = require('crypto');
const app = express();

let users = [
  {
    id: 1,
    name: 'Admin',
    sname: 'Sname',
    email: 'admin@sctrade.es',
    username: 'admin',
    password: 'admin',
    token: ''
  }
];

let courses = [
  {
    id: '1',
    name: 'Angular curso básico',
    level: 'Básico',
    teacher: 'Gerard Ortega',
  },
  {
    id: '2',
    name: 'Angular curso avanzado',
    nivel: 'Avanzado',
    teacher: 'Gerard Ortega',
  }
];


app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.route('/api/users')
  .get((req, res) => {
    res.status(200).json(users);
  });

app.route('/api/courses')
  .get((req, res) => {
    res.status(200).json(courses);
  });

app.route('/api/course')
  .get((req, res) => {
    const course = courses.find(c => c.id === req.query.id);
    res.status(200).json(course);
  });


app.route('/api/login').post((req, res) => {
  const user = users.find(u => u.username === req.body.username && u.password === req.body.password);
  console.log(req.body);
  if (user) {
    const hash = encrypt.createHmac('sha256', JSON.stringify(user))
      .digest('hex');
    user.token = hash;
    res.status(200).json({ 'token': hash, user });
  } else {
    res.status(401).json({ 'error': 'User invalid' });
  }
});











app.route('/api/edit').put((req, res) => {
  const user = req.body.user;
  const serverUser = users.find(u => u.id === user.id);
  serverUser.username = user.username;
  serverUser.password = user.password;
  serverUser.name = user.name;
  serverUser.sname = user.sname;
  serverUser.email = user.email;
  res.status(200).json(users);
});
app.route('/api/create').post((req, res) => {
  const user = req.body.user;
  user.url = 'user.png';
  user.id = Math.max.apply(Math, users.map(u => u.id)) + 1;
  users.push(user);
  res.status(200).json(users);
});
app.route('/api/remove').delete((req, res) => {
  users = users.filter(u => u.id !== Number(req.query.id));
  res.status(200).json(users);
});
app.listen(3000, () => {
  console.log('Server running on port 3000');
});
